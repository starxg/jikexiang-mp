import Taro from "@tarojs/taro";

const logger = Taro.getRealtimeLogManager();

export const info = (...args: any[]) => {
  if (logger) {
    logger.info(args)
  } else {
    console.error(args)
  }
}

export const error = (...args: any[]) => {
  if (logger) {
    logger.error(args)
  } else {
    console.error(args)
  }
}
