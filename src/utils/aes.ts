import c from 'crypto-js'


const aes = {
  encrypt: (key: string, iv: string, data: string) => {
    const ikey = c.enc.Utf8.parse(key);
    const iiv = c.enc.Utf8.parse(iv);
    const idata = c.enc.Utf8.parse(data);
    const option = {
      iv: iiv,
      mode: c.mode.CBC,
      padding: c.pad.Pkcs7
    };
    return c.AES.encrypt(idata, ikey, option).toString();
  },
  decrypt: (key: string, iv: string, data: string) => {
    const ikey = c.enc.Utf8.parse(key);
    const iiv = c.enc.Utf8.parse(iv);
    const option = {
      iv: iiv,
      mode: c.mode.CBC,
      padding: c.pad.Pkcs7
    };

    const decrypt = c.AES.decrypt(data, ikey, option);
    return c.enc.Utf8.stringify(decrypt).toString();
  },
  sha256(data: string) {
    return c.enc.Hex.stringify(c.SHA256(data)).toString()
  },
  hmacsha256(data: string, key: string) {
    return c.enc.Hex.stringify(c.HmacSHA256(data, key)).toString()
  },
  md5(data: string) {
    return c.enc.Hex.stringify(c.MD5(data)).toString()
  }
};

export {
  aes
}
