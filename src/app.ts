import {createApp} from 'vue'
import {createPinia} from 'pinia'
import {createPersistedState} from 'pinia-plugin-persistedstate'

import './app.scss'
import Taro from "@tarojs/taro";
import {useAppStore} from "./stores/counter";
import {error} from "./utils/realtime-log";

const App = createApp({
  onShow() {
    Taro.getSystemInfo().then(e => {
      useAppStore().setSystemInfo(e)
    }).catch(error)
  },
  onLaunch() {
    console.log('onLaunch')
  }
})

const pinia = createPinia();

pinia.use(createPersistedState({
  auto: true,
  storage: {
    setItem: (key: string, value: string) => {
      try {
        Taro.setStorageSync(key, value)
      } catch (e) {
        error(`设置持久化[${key}]失败`, value, e)
      }
    },
    getItem: (key: string) => {
      try {
        return Taro.getStorageSync(key)
      } catch (e) {
        error(`获取持久话[${key}]失败`, key, e)
        return null
      }
    },
  }
}))
App.use(pinia)

export default App
