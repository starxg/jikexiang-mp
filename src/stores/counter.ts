// https://pinia.esm.dev/introduction.html
import {defineStore} from 'pinia'

export const useCounterStore = defineStore('counter', {
  persist: false,
  state: () => {
    return {count: 0}
  },
  actions: {
    increment() {
      this.count++
    },
  },
})

export const useAppStore = defineStore('app', {
  state: () => {
    return {
      systemInfo: {} as any
    }
  },
  actions: {
    setSystemInfo(systemInfo: any) {
      this.systemInfo = systemInfo
      console.log(systemInfo)
    },
  }
})

// You can even use a function (similar to a component setup()) to define a Store for discovery advanced use cases:
// export const useCounterStore = defineStore('counter', () => {
//   const count = ref(0)
//
//   function increment() {
//     count.value++
//   }
//
//   return {count, increment}
// })
