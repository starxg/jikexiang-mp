export default defineAppConfig({
  pages: [
    'pages/index/index',
    'pages/user/my/index',
    'pages/user/profile/index',
    'pages/discovery/index',
    'pages/share/detail/index',
    'pages/share/add/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  },
  tabBar: {
    list: [
      {
        iconPath: "assets/homeFolder.png",
        selectedIconPath: "assets/homeFolder.png",
        text: "首页",
        pagePath: "pages/index/index"
      },
      {
        iconPath: "assets/homeFolder.png",
        selectedIconPath: "assets/homeFolder.png",
        text: "发现",
        pagePath: "pages/discovery/index"
      },
      {
        iconPath: "assets/homeFolder.png",
        selectedIconPath: "assets/homeFolder.png",
        text: "我的",
        pagePath: "pages/user/my/index"
      }
    ]
  },
  lazyCodeLoading: "requiredComponents"
})
